<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::any('/register','RegisterController@index');
Route::post('/saveusers','RegisterController@saveuser');
Route::any('/login','RegisterController@login');
Route::any('/logout','RegisterController@logout');
Route::post('/logincheck','RegisterController@logincheck');
Route::any('/users','RegisterController@getusers');
Route::any('/useredit/{id}','RegisterController@edituser');
Route::any('/userupdate/{id}','RegisterController@updateuser');

