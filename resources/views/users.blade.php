<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
         
        <div class="top-right links">
                    <?php if (Sentinel::check()) { ?>
                        <a href="{{ url('/users') }}">Home</a>
                        <a href="{{ url('/logout') }}">Logout</a>
                    <?php } else { ?>
                        <a href="{{ url('/login') }}">Login</a>
                        <a href="{{ url('/register') }}">Register</a>
                    <?php } ?>
                </div>
          
        <table>
                    @if ($message = Session::get('successdel'))
<div class="alert alert-success alert-block">	
        <strong>{{ $message }}</strong>
</div>
@endif
        <?php //echo '<pre>';print_r($usersdata);?>
        @if(isset($usersdata))
               @foreach($usersdata as $user)
               <tr>
            
                <td>{{$user->email}}</td>
                <td>{{$user->first_name}}</td>
                 <td>{{$user->last_name}}</td>
                 <td><a href="useredit/{{$user->id}}">Edit</a></td>
                 <td><a href="userdelete/{{$user->id}}">Delete</a></td>
            </tr>
            @endforeach
            @endif
    </table>
    </body>
</html>
