<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Sentinel;
use DB;
use Illuminate\Http\Request;

class RegisterController extends Controller {

public function index() {

return view('form');
}

public function saveuser(Request $request) {
//echo 'test';
//dd($request->all());die;
$user = Sentinel::registerAndActivate($request->all());
return redirect('/')->with('register', 'register in successfully!');
}

public function login() {

return view('login');
}

public function logincheck(Request $request) {

$data = $request->all();
$credentials = ['email' => $data['email'], 'password' => $data['password']];
// echo '<pre>';print_r($credentials);die;
$user = Sentinel::authenticate($credentials);
//If Authentication was successful
if (!empty($user)) {
//  echo 'Test';
//Login without remember
Sentinel::login($user);
//  echo 'Test111';
}
return redirect('/')->with('login', 'login in successfully!');
}

public function logout(Request $request) {
Sentinel::logout();
return redirect('/');
}

public function getusers(Request $request) {
$usersdata = DB::table('users')->select('id', 'email', 'first_name', 'last_name')->get();
//echo '<pre>';print_r($users);die;
return view('users', ['usersdata' => $usersdata]);
}
public function edituser(Request $request, $userid) {
//  $data = $request->all();
// echo '<pre>';print_r( $data);die;
$usersdata = DB::table('users')->select('id', 'email', 'first_name', 'last_name')->where('id', $userid)->first();
//echo '<pre>';print_r($users);die;
return view('useredit', ['usersdata' => $usersdata]);
}

public function updateuser(Request $request, $userid) {
$data = $request->all();
$updateuser = ['first_name' => $data['first_name'], 'last_name' => $data['last_name']];
$updatequery = DB::table('users')->where('id', $userid)->update($updateuser);

if($updatequery){
    return redirect('/useredit/'.$userid)->with('updated', 'user updated successfully!');

}
else{
 return redirect('/useredit/'.$userid)->with('updatedf', 'user updated failed!');
}
}

public function deleteuser(Request $request, $userid) {
        $delquery = DB::table('users')->where('id', $userid)->delete();
        if($delquery){
        return redirect()->back()->with('successdel', 'Deleted Successfullty');  
        } else{
            
        }
    
}
}

